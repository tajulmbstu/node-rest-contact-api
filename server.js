const express = require('express')
const app = express()
const bodyParser = require('body-parser')
var cors = require('cors')
// import all route const
const contactRoute = require('./api/routes/contactsRoute')
const userRoute = require('./api/routes/usersRoute')
// Database connection
const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
// uri for localhost connection
const uri = 'mongodb://localhost/contact'

// uri for mongo atlas online cloude connection
// const uri = 'mongodb+srv://admin:admin@cluster0-uywan.mongodb.net/contact?retryWrites=true&w=majority'

mongoose.connect(uri,{ useNewUrlParser: true });

const db = mongoose.connection;
db.on('error', (err)=>{
    console.log(err)
});
db.once('open', ()=> {
  // we're connected!
  console.log("Database Connection Successful")
});

//  db end

const PORT = process.env.PORT || 3000


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse application/json
app.use(bodyParser.json())
// cors
app.use(cors())

app.use('/api/user', userRoute)
app.use('/api/contact', contactRoute)
app.use('/uploads',express.static('uploads'))

app.get('/', (req, res) => res.send('Welcome to my Contact rest api'))


app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`))


