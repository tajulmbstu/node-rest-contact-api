const jwt = require('jsonwebtoken');

const authenticate = (req, res, next)=>{
    try{
        // this is Bearer token. Bearer is in Array[0] and split the token by a space
        const token = req.headers.authorization.split(' ')[1]
        const decode = jwt.verify(token, 'SECRET')
    
        req.user = decode
        next()
    }
    catch(err){
        res.json({
            message:"Authentication Failed"
        })
    }
}

module.exports= authenticate