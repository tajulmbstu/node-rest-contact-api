const mongoose = require('mongoose')
const Schema = mongoose.Schema
const validator = require('validator')
var uniqueValidator = require('mongoose-unique-validator')

const userSchema = new Schema({
    email:{
        type:String,
        trim:true,        
        required:true,
        validate:{
            validator: (v) => {
                return validator.isEmail(v);
            },
            message:`{VALUE} is not an email`
        },
        unique:true
    },
    password:{
        type:String
    }

},{ collection: 'user' })

// Apply the uniqueValidator plugin to userSchema.
userSchema.plugin(uniqueValidator)

const UserModel = mongoose.model('User',userSchema)

// Here Contact is my model name which is created in db collection/table name as [ users ] 
// because of mongoose default behaviour ( convert all small letter and add an extra s with the 
// collection name). If we want to create the same name in db then we need to add the collection 
// name at the end of the schema like 

//      const contactSchema = new Schema({},{ collection: 'User' })

module.exports = UserModel
