const mongoose = require('mongoose')
const Schema = mongoose.Schema
const validator = require('validator');
var uniqueValidator = require('mongoose-unique-validator');


const contactSchema = new Schema({
    name:{
        type:String,
        trim:true,
        required:true,
        minlength:5
    },
    email:{
        type:String,
        trim:true,        
        required:true,
        validate:{
            validator: (v) => {
                return validator.isEmail(v);
            },
            message:`{VALUE} is not an email`
        },
        unique:true
    },
    image_url:{
        type:String,
        required:true
    }

},{ collection: 'contact' })

contactSchema.plugin(uniqueValidator);

const ContactModel = mongoose.model('Contact',contactSchema)

// Here Contact is my model name which is created in db collection/table name as [ contacts ] 
// because of mongoose default behaviour ( convert all small letter and add an extra s with the 
// collection name). If we want to create the same name in db then we need to add the collection 
// name at the end of the schema like 

//      const contactSchema = new Schema({},{ collection: 'Contact' })


module.exports = ContactModel;