const ContactModel = require('../model/ContactModel')

// GET All contact
const getAllContactController = (req, res, next) =>{
    ContactModel.find()
        .then(contacts=>{
            // res.status(200).json(contacts)
            res.status(200).json(
                // message:'All Contacts',
                contacts
            )
        })
        .catch(err=>{
            res.status(500).json({
                message: 'Error occured',
                err: err
            })
        })
}
// Post new contact
const postNewContactController = (req, res, next) =>{
    // console.log(req.file)
    
    const name = req.body.name
    const email = req.body.email
    const image_url = "http://localhost:3000/"+ req.file.path.replace(/\\/g,"/")
    
    const contactdata = new ContactModel({
        name: name,
        email: email,
        image_url: image_url
    })

    contactdata.save()
        .then( data => {
            res.status(201).json({
                message:'Contact added',
                contact: data
            })
        })
        .catch( err => {
            console.log(err)
            res.status(500).json({
                message: 'Error occured',
                err: err
            })
        })
}

// Get single contact
const getSinglecontact = (req, res, next) =>{
    const id = req.params.id
    ContactModel.findById(id)
        .then(contact =>{
            if(!contact){
                return res.status(404).end()
            }
            return res.status(200).json(contact)
        })
        .catch( err => {
            console.log(err)
            res.status(500).json({
                message: 'Error occured',
                err: err
            })
        } )
}
// delete single contact
const deleteSingleContact = (req, res, next)=>{
    const id = req.params.id
    ContactModel.findByIdAndRemove(id)
        .then(result =>{
            res.json({
                message:"contact deleted",
                result
            })
        })
        .catch(err =>{
            console.log(err)
            res.json({
                message:"error occured",
                error: err
            })
        })
}
// Update / edit a contact
const editContact = (req,res,next)=>{
    const id = req.params.id
    let updatedContact = {
        name:req.body.name,
        email:req.body.email
    }
    ContactModel.findByIdAndUpdate(id, {$set:updatedContact})
        .then(contact =>{
            res.json({
                message:"Updated Successfully",
                oldContact:contact
            })
        })
        .catch(err =>{
            console.log(err)
            res.json({
                message:"error occured",
                error: err
            })
        })
}
// --------------
module.exports = {
    getAllContactController,
    postNewContactController,
    getSinglecontact,
    deleteSingleContact,
    editContact
}