const express = require('express')
const router = express.Router()
const multer  = require('multer')

const authenticate = require('../middleware/authentication')
const contractController = require('../controllers/contactController')

// image file storage
const storage = multer.diskStorage({
  
  destination: function(req, file, cb){
    cb(null, './uploads/');
  },
  
  filename: function(req, file, cb){
    cb(null, new Date().getTime() + '-' + file.originalname);
  }
}); 
// file filter
const fileFilter = (req, file, cb) =>{
    // reject file 
    if(file.mimetype === 'image/jpeg' || file.mimetype ==='image/png'){
      cb(null, true)
    }else{
      cb(null, false)
    }
};
// file (image) upload 
const upload = multer({ 
  storage: storage, 
  limits:{
    // file size max 5 mb or (1024*1024*5) byte
    fileSize: 1024*1024*5
  },
  fileFilter : fileFilter
 })
//  end image file upload and store


// GET all
router.get('/',contractController.getAllContactController)

// POST all
  router.post('/', authenticate, upload.single('file'), contractController.postNewContactController)
  

// id GET 
router.get('/:id', contractController.getSinglecontact)

// id PUT
router.put('/:id', authenticate, contractController.editContact)

// id DELETE
router.delete('/:id', authenticate, contractController.deleteSingleContact)


  module.exports = router;