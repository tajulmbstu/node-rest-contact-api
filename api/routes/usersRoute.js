const express = require('express')
const router = express.Router()
const usersController = require('../controllers/usersController')
const authenticate = require('../middleware/authentication')


// 
router.post('/register',usersController.registerController)

// 
router.get('/userList', authenticate, usersController.getAllUser)

// 
router.post('/login', usersController.logInController)

module.exports = router